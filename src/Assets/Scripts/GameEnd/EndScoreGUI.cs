﻿using UnityEngine;
using System.Collections;

public class EndScoreGUI : MonoBehaviour {
	
	public GUIText StudentCountText;
	public GUIText ManCountText;
	public GUIText WomanCountText;
	public GUIText SantaCountText;
	public GUIText TotalCountText;
	public GUIText IncomeText;
	
	float sr = 2f / 3;
	
	// Use this for initialization
	void Start () {
		StartCoroutine (WriteToUI());
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	IEnumerator WriteToUI() {
		int iTotal = 0;
		
		Debug.Log (StageManager.pScoreData.IntValue());
		
		{
			int iStudent = StageManager.pScoreData.iStudent, iStudent0 = 0;
			while (iStudent != 0) {
				int inc = (int) (iStudent * sr + 0.5);
				Debug.Log (string.Format ("{0} {1}", iStudent, inc));
				iStudent0 += inc;
				iTotal += inc;
				iStudent -= inc;
				StudentCountText.text = iStudent0.ToString();
				TotalCountText.text = iTotal.ToString();
				yield return new WaitForSeconds(0.1f);
			}
		}
		
		{
			int iMan = StageManager.pScoreData.iMan, iMan0 = 0;
			while (iMan != 0) {
				int inc = (int) (iMan * sr + 0.5);
				iMan0 += inc;
				iTotal += inc;
				iMan -= inc;
				ManCountText.text = iMan0.ToString();
				TotalCountText.text = iTotal.ToString();
				yield return new WaitForSeconds(0.1f);
			}
		}
		
		{
			int iWoman = StageManager.pScoreData.iWoman, iWoman0 = 0;
			while (iWoman != 0) {
				int inc = (int) (iWoman * sr + 0.5);
				iWoman0 += inc;
				iTotal += inc;
				iWoman -= inc;
				WomanCountText.text = iWoman0.ToString();
				TotalCountText.text = iTotal.ToString();
				yield return new WaitForSeconds(0.1f);
			}
		}
		
		{
			int iSanta = StageManager.pScoreData.iSanta, iSanta0 = 0;
			while (iSanta != 0) {
				int inc = (int) (iSanta * sr + 0.5);
				iSanta0 += inc;
				iTotal += inc;
				iSanta -= inc;
				SantaCountText.text = iSanta0.ToString();
				TotalCountText.text = iTotal.ToString();
				yield return new WaitForSeconds(0.1f);
			}
		}
		
		{
			int iValue = StageManager.pScoreData.IntValue(), iValue0 = 0;
			while (iValue != 0) {
				int inc = (int) (iValue * sr + 0.5);
				iValue0 += inc;
				iValue -= inc;
				IncomeText.text = iValue0.ToString();
				TotalCountText.text = iTotal.ToString();
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}
