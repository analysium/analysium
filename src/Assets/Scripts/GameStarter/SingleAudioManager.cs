﻿using UnityEngine;
using System.Collections;

public class SingleAudioManager : MonoBehaviour {
	
	public AudioSource[] AudioSourceRef;
	
	int i;

	// Use this for initialization
	void Start () {
		i = Random.Range (0, AudioSourceRef.Length);
		
		AudioSourceRef[i].loop = true;
		AudioSourceRef[i].Play ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Stop () {
		AudioSourceRef[i].Stop ();
	}
}
