﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NPCManager : MonoBehaviour {
	
	public GameObject GaugeObj;
	public GameObject ScoreObj;
	
	public GameObject PoliceNPCDownPrototype;
	public GameObject WorkmanNPCPrototype;
	public GameObject StudentNPCPrototype;
	public GameObject WorkwomanNPCPrototype;
	public GameObject ChildNPCPrototype;
	public GameObject SantaNPCPrototype;
	public GameObject OldmanNPCPrototype;
	
	public GameAudioEffectManager AFXManager;
	
	float Speed = 0.5f;
	
	private List<NPC> list = new List<NPC>();
	[SerializeField]
	public List<NPC> List { get { return list; } }
	
	private List<NPC.Builder> NPCBuilders;
	
	float time_Police;
	float time_NPC;
	public float PolicePeriod = 0.3f;
	public float WorkmanPeriod = 0.3f;
	public float StudentPeriod = 0.3f;
	public float WorkwomanPeriod = 0.3f;
	public float ChildPeriod = 0.3f;
	public float SantaPeriod = 0.3f;
	public float OldmanPeriod = 0.3f;

	// Use this for initialization
	void Start () {
		Run = true;
		
		NPCBuilders = new List<NPC.Builder> {
			new PoliceNPCDown.Builder(),
			new WorkmanNPC.Builder(),
			new WorkwomanNPC.Builder(),
			new StudentNPC.Builder(),
			new ChildNPC.Builder(),
			new SantaNPC.Builder(),
			new OldmanNPC.Builder()
		};
	}
	
	private bool run;
	public bool Run {
		get { return run; }
		set {
			this.run = value;
			foreach (var NPC in List) {
				NPC.Run = this.run;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		List<NPC> removeList = new List<NPC>();
		foreach (var builder in NPCBuilders) {
			if (Run && builder.on (this)) {
				NPC NPC = builder.create (this);
				NPC.Run = true;
				list.Add (NPC);
			}
		}
		foreach (var NPC in list) {
			if (NPC.update() == false)
				removeList.Add (NPC);
		}
		foreach (var NPC in removeList) {
			NPC.destroy();
			list.Remove (NPC);
		}
	}
	
	readonly float[] judge = {0.2f, 0.2f, 0.16f, 0.14f, 0.2f};
	readonly float[] miss = {0.25f, 0.25f, 0.25f, 0.25f, 0.25f};
	
	public void StealApproachFromRight(Vector3 pos) {
		foreach (var NPC in List) {
			if (isLeftApproach(NPC.transform.localPosition, pos) &&
					isValidLeftApproach(NPC.transform.localPosition, pos, NPC.type())) {
				
				TrySteal(NPC, 1);
			
			} else if(isLeftApproach (NPC.transform.localPosition, pos) &&
					isFailedLeftApproach(NPC.transform.localPosition, pos, NPC.type())) {
				
				FailSteal(NPC);
				
			}
		}
	}
	
	public void StealApproachFromLeft(Vector3 pos) {
		foreach (var NPC in List ){
			if (isRightApproach (NPC.transform.localPosition, pos) &&
					isValidRightApproach (NPC.transform.localPosition, pos, NPC.type())) {
				
				TrySteal(NPC, 2);
			
			} else if (isRightApproach (NPC.transform.localPosition, pos) &&
					isFailedRightApproach (NPC.transform.localPosition, pos, NPC.type())) {
				
				FailSteal(NPC);
				
			}
		}
	}
	
	void TrySteal(NPC NPC, int flag) {
		int type = NPC.type ();
		int val = NPC.onStolen(flag);
		
		StageManager.pScoreData.IncreaseOfType(type);
		if (val == -1) {
			GaugeObj.GetComponent<GaugeController>().GaugeState ++;
			NPC.AddExclamationMark();
//			if (!GaugeObj.GetComponent<GaugeController>().IsGaugeFull())
			AFXManager.Fail();
		} else {
			ScoreObj.GetComponent<ScoreController>().Score = StageManager.pScoreData.IntValue();
			NPC.AddQuestionMark();
			AFXManager.Success();
		}
	}
	
	void FailSteal(NPC NPC) {
		GaugeObj.GetComponent<GaugeController>().GaugeState ++;
		NPC.AddExclamationMark();
//		if (!GaugeObj.GetComponent<GaugeController>().IsGaugeFull())
		AFXManager.Fail();
	}
	
	bool isLeftApproach(Vector3 NPCPos, Vector3 PCPos) {
		return PCPos.x - NPCPos.x <= 0.126 && PCPos.x - NPCPos.x >= 0.1;
	}
	
	bool isValidLeftApproach(Vector3 NPCPos, Vector3 PCPos, int type) {
		return NPCPos.y >= 0.18f && NPCPos.y <= 0.2f + judge[type];
	}
	
	bool isFailedLeftApproach(Vector3 NPCPos, Vector3 PCPos, int type) {
		return NPCPos.y >= 1.8f && NPCPos.y <= 0.2f + miss[type];
	}
	
	bool isRightApproach(Vector3 NPCPos, Vector3 PCPos) {
		return NPCPos.x - PCPos.x <= 0.126 && NPCPos.x - PCPos.x >= 0.1;
	}
	
	bool isValidRightApproach(Vector3 NPCPos, Vector3 PCPos, int type) {
		return NPCPos.y >= 0.18f && NPCPos.y <= 0.2f + judge[type];
	}
	
	bool isFailedRightApproach(Vector3 NPCPos, Vector3 PCPos, int type) {
		return NPCPos.y >= 1.8f && NPCPos.y <= 0.2f + miss[type];
	}
}
