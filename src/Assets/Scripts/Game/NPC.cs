using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class NPC : Character
{
	public static GameObject pExclamationMarkObj;
	public static GameObject pQuestionMarkObj;
	
	public bool Run { get; set; }
	
	public float NPC_downSpeed = 1.5f;
	public static float GetRandPeriod()
	{
		return (Random.value * 6) + 1;
	}
	
	public NPC ()
	{
	}
	
	public void destroy() {
		gameObject.SetActive(false);
		GameObject.Destroy(gameObject);
	}
	
	public void AddExclamationMark() {
		Vector3 prePos;
		GameObject exclamationMark = (GameObject)GameObject.Instantiate(pExclamationMarkObj);
		exclamationMark.SetActive(true);
		prePos = this.transform.position;
		prePos.x += 0.05f;
		prePos.y += 0.1f;
		prePos.z = 16;
		exclamationMark.transform.position = prePos;
		exclamationMark.transform.parent = this.transform;
	}
	
	public void AddQuestionMark() {
		Vector3 prePos;
		GameObject questionMark = (GameObject)GameObject.Instantiate(pQuestionMarkObj);
		questionMark.SetActive(true);
		prePos = this.transform.position;
		prePos.x += 0.05f;
		prePos.y += 0.1f;
		prePos.z = 16;
		questionMark.transform.position = prePos;
		questionMark.transform.parent = this.transform;
	}
	
	public abstract int type();
	public abstract int onStolen(int type);
	
	public abstract bool update();
	
	public abstract class Builder {
		public abstract void init();
		public abstract bool on(NPCManager manager);
		public abstract NPC create(NPCManager manager);
	}
}

