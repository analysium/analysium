﻿using UnityEngine;
using System.Collections;

public class ScoreController : MonoBehaviour
{
	public int Score { get; set; }
	public GUIStyle Style = new GUIStyle();
	
	void OnGUI()
	{
		Rect labelPos = new Rect(0, 0, Screen.width * 0.92f, Screen.height * 0.1f);
		GUI.Label(labelPos, Score.ToString(), Style);
	}
}
