﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GaugeController : MonoBehaviour
{
	public PlayerCharacter p;
	
	public int GaugeState
	{
		get { return gaugeState; }
		set
		{
			// 유효 범위 체크.
			if (value < 0 || value >= GaugeTextures.Count)
				return;
			
			GUITexture guiTex = gameObject.GetComponent<GUITexture>();
			guiTex.texture = GaugeTextures[value];
			gaugeState = value;
			lastGaugeChangedTime = Time.time;
		}
	}
	
	public List<Texture> GaugeTextures = new List<Texture>();
	public float DecreasePeriod = 5.0f;
	
	public const int GAUGE_LEN = 4;
	
	int gaugeState;
	float lastGaugeChangedTime;
	
	public bool IsGaugeFull() {
		return gaugeState == GAUGE_LEN;
	}
	
	void Start ()
	{
		GaugeState = 0;
	}
	
	void Update () {
		if (GaugeState > 0 && Time.time - lastGaugeChangedTime >= DecreasePeriod) {
			GaugeState = GaugeState - 1;
		}
		
		if (GaugeState == GAUGE_LEN)
			p.DoWaitAndEnd();
	}
}
