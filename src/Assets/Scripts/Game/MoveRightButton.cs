﻿using UnityEngine;
using System.Collections;

public class MoveRightButton : GUIButton {
	
	public PlayerCharacter PlayerCharacter;

	public override void OnClick ()
	{
		PlayerCharacter.MoveRight();
	}
	
}
