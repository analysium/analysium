﻿using UnityEngine;
using System.Collections;

public class GrabLeftButton : GUIButton {
	
	public PlayerCharacter PlayerCharacter;

	public override void OnClick ()
	{
		PlayerCharacter.GrabLeft();
	}
	
}
