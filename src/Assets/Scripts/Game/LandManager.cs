﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LandManager : MonoBehaviour {
	
	public GUITexture BaseTexture;
	public GameObject Self;
	public List<GUITexture> TextureList = new List<GUITexture>();
	public bool Run { get; set; }
	
	[SerializeField]
	public float PageTime = 1.0f;

	// Use this for initialization
	void Start () {
		
		Run = true;
		
		BaseTexture.enabled = false;
		
		GUITexture texture1 = GUITexture.Instantiate(BaseTexture) as GUITexture;
		texture1.transform.parent = Self.transform;
		texture1.enabled = true;
		TextureList.Add (texture1);
		
		GUITexture texture2 = GUITexture.Instantiate(BaseTexture) as GUITexture;
		texture2.transform.parent = Self.transform;
		texture2.transform.localPosition += new Vector3(0.0f, 1.0f, 0.0f);
		texture2.enabled = true;
		TextureList.Add (texture2);
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Run) {
			foreach (GUITexture texture in TextureList) {
				texture.transform.localPosition += new Vector3(0.0f, -1.0f, 0.0f) * Time.deltaTime / PageTime;
				if (texture.transform.position.y < -0.5f)
					texture.transform.localPosition += new Vector3(0.0f, 2.0f, 0.0f);
			}
		}
	}
}
