﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCharacter : Character
{
	public float AniPeriod = 0.2f;
	public float StealDuration = 0.2f;
	public NPCManager NPCManagerRef;
	public LandManager LandManagerRef;
	public GameObject QuestionMarkObj;
	public GameObject ExclamationMarkObj;
	public GameAudioEffectManager AFXManager;
	public SingleAudioManager BackgroundSoundManager;
	
	public bool Run { get; set; }
	
	int track = 2;
	
	int aniIndex = 0;
	float time_check;
	bool deActive = false;
	public GameObject[] anis_walk;
	public GameObject[] anis_catch; //0: left, 1: right;
	public GameObject Self;
	
	// Use this for initialization
	void Start ()
	{
//		NPCGeneratorObj.GetComponent<NPCGenerator>().List;
		aniIndex = 0;
		track = 2;
		
		StageManager.pScoreData = new StageManager.ScoreData();
		
		Run = true;
		
		for (int i = 0; i < anis_catch.Length; i++)
			anis_catch[i].GetComponent<GUITexture>().enabled = false;
		
		Active(aniIndex);
		
		NPC.pExclamationMarkObj = ExclamationMarkObj;
		NPC.pQuestionMarkObj = QuestionMarkObj;
	}
	
	// Update is called once per frame
	void Update () {
		if (!Run) return;
		aniIndex = (int)(Time.time / AniPeriod) % anis_walk.Length;
		Active(aniIndex);
		
		foreach (var NPC in NPCManagerRef.List) {
			if (NPC.transform.position.y >= 0.2f && NPC.transform.position.y <= 0.3f){
				if (gameObject.transform.position.x == NPC.transform.position.x){
					Debug.Log ("Yaranaika!");
					
					DoWaitAndEnd();
				}
			}
		}
		
		if (deActive) {
			if ((float)(Time.time - time_check) >= StealDuration) {
				deActive = false;
				for (int i = 0; i < anis_catch.Length; i++)
					anis_catch[i].GetComponent<GUITexture>().enabled = false;
			}
			else return;
		}
			
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			MoveLeft ();
		}
		if (Input.GetKeyDown(KeyCode.RightArrow)) {
			MoveRight ();
		}
		if (Input.GetKeyDown(KeyCode.Q)) {
			GrabLeft ();
		}
		if (Input.GetKeyDown(KeyCode.E)) {
			GrabRight ();
		}
	}
	
	public void MoveLeft() {
		if (deActive)
			return;
		
		if (track != 1){
			--track;
			Self.transform.position += new Vector3(PosX(-1), 0, 0);
			AFXManager.Dash();
		}
	}
	
	public void MoveRight() {
		if (deActive)
			return;
		
		if (track != 3) {
			++track;
			Self.transform.position += new Vector3(PosX(+1), 0, 0);
			AFXManager.Dash();
		}
	}
	
	public void GrabLeft() {
		if (deActive)
			return;
		
		deActive = true;
		time_check = Time.time;
		anis_walk[aniIndex].GetComponent<GUITexture>().enabled = false;
		anis_catch[1].GetComponent<GUITexture>().enabled = true;
		
		NPCManagerRef.StealApproachFromRight(gameObject.transform.localPosition);
	}
	
	public void GrabRight() {
		if (deActive)
			return;
		
		deActive = true;
		time_check = Time.time;
		anis_walk[aniIndex].GetComponent<GUITexture>().enabled = false;
		anis_catch[0].GetComponent<GUITexture>().enabled = true;
		
		NPCManagerRef.StealApproachFromLeft(gameObject.transform.localPosition);
	}
	
	float PosX(int track) {
		return track * 0.25f;
	}
	
	void Active(int aniIdx)
	{
		if (deActive) return;
		for (int i = 0; i < anis_walk.Length; i++)
			anis_walk[i].GetComponent<GUITexture>().enabled = (i == aniIdx);
	}
	
	public void DoWaitAndEnd()
	{
		if (Run)
			StartCoroutine(WaitAndEnd());
	}
	
	public IEnumerator WaitAndEnd()
	{
		this.Run = false;
		NPCManagerRef.Run = false;
		LandManagerRef.Run = false;
		
		AFXManager.Die();
		BackgroundSoundManager.Stop();
		
		yield return new WaitForSeconds(4.0f);
		Application.LoadLevel("GameEnd");
	}
}
