﻿using UnityEngine;
using System.Collections;

public class GrabRightButton : GUIButton {
	
	public PlayerCharacter PlayerCharacter;

	public override void OnClick ()
	{
		PlayerCharacter.GrabRight();
	}
	
}
