﻿using UnityEngine;
using System.Collections;

public class MoveLeftButton : GUIButton {
	
	public PlayerCharacter PlayerCharacter;

	public override void OnClick ()
	{
		PlayerCharacter.MoveLeft();
	}
	
}
