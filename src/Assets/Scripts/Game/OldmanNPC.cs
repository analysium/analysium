﻿using UnityEngine;
using System.Collections;

public class OldmanNPC : NPC {
	
	private static float pGenTimeDist;
	public GameObject[] anis;
	public int status;
	
	float aniPeriod = 0.2f;
	
	int aniIndex;
	Vector3 prePos;

	// Use this for initialization
	void Start () {
		float rand = Random.value;
		prePos = new Vector3(0.125f,1.3f,14.0f);
		if (rand<=0.25f) prePos.x +=0.25f;
		else if (rand<=0.50f)prePos.x += 0.50f;
		else if (rand<=0.75f)prePos.x += 0.75f;
		this.transform.position = prePos;
		
		for (int i = 0; i < 4; i++) anis[ i ].GetComponent<GUITexture>().enabled = false;
		
		status = 0;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!Run) return;
		if (aniIndex != -1) { 
			aniIndex = (int)(Time.time / aniPeriod) % anis.Length;
			Active(aniIndex);
		}
		prePos.y -= (float)(1/NPC_downSpeed)/60.0f;
		this.transform.position = prePos;
	}
	
	void Active(int aniIdx) {
		for (int i = status; i < status + 2; i++)
			anis[i].GetComponent<GUITexture>().enabled = (i%2 == aniIdx%2);
	}
	
	public override int onStolen(int type){
		if (status != 2) {
			for (int i = status; i < status + 2; i++)
				anis[i].GetComponent<GUITexture>().enabled = false;
			status = 2;
		}
		return -1;
	}
	public override int type() {
		return 0;
	}
	
	public override bool update() {
		if (gameObject.transform.position.y <= -0.3f) {
			aniIndex = -1;
			return false;
		}
		return true;
	}
	
	public class Builder : NPC.Builder {
		public override void init() {
			pGenTimeDist = 0;
		}
		public override bool on(NPCManager manager) {
			if (Time.time - pGenTimeDist > manager.OldmanPeriod) {
				manager.OldmanPeriod = NPC.GetRandPeriod();
				pGenTimeDist = Time.time;
				return true;
			}
			return false;
		}
		public override NPC create(NPCManager manager) {
			var obj = GameObject.Instantiate(manager.OldmanNPCPrototype) as GameObject;
			Debug.Log (obj);
			obj.SetActive(true);
			return obj.GetComponent<NPC>();
		}
	}
}
