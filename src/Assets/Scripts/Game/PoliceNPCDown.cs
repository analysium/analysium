﻿using UnityEngine;
using System.Collections;

public class PoliceNPCDown : NPC {
	
	private static float pGenTimeDist;
	public GameObject[] anis;
	
	public float AniPeriod = 0.4f;
	float Police_downSpeed = 0.8f;
	
	int aniIndex;
	Vector3 prePos;

	// Use this for initialization
	void Start () {
		float rand = (float)(Random.value);
		prePos = new Vector3(0.5f,1.3f,14.0f);
		if (rand<=0.33f) prePos.x -=0.25f;
		else if (rand<=0.66f)prePos.x += 0.25f;
		this.transform.position = prePos;
	}
	
	// Update is called once per frame
	void Update () {
		if (!Run) return;
		if (aniIndex != -1) {
			aniIndex = (int)(Time.time / AniPeriod) % anis.Length;
			Active(aniIndex);
		}
		prePos.y -= (float)(1/Police_downSpeed)/60.0f;
		this.transform.position = prePos;
	}
	
	void Active(int aniIdx) {
		for (int i = 0; i < anis.Length; i++)
			anis[i].GetComponent<GUITexture>().enabled = (i == aniIdx);
	}
	
	public override int onStolen(int type){
		return 0;
	}
	public override int type(){
		return 0;
	}
	
	public override bool update() {
		if (gameObject.transform.position.y <= -0.3f) {
			aniIndex = -1;
			return false;
		}
		return true;
	}
	
	public class Builder : NPC.Builder {
		public override void init() {
			pGenTimeDist = 0;
		}
		public override bool on(NPCManager manager) {
			if (Time.time - pGenTimeDist > manager.PolicePeriod) {
				manager.PolicePeriod = NPC.GetRandPeriod();
				pGenTimeDist = Time.time;
				return true;
			}
			return false;
		}
		public override NPC create(NPCManager manager) {
			var obj = GameObject.Instantiate(manager.PoliceNPCDownPrototype) as GameObject;
			Debug.Log (obj);
			obj.SetActive(true);
			return obj.GetComponent<NPC>();
		}
	}
}
