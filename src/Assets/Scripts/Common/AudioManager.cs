﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
	
	public struct AudioEntity {
		public string Name { get; set; }
		public AudioSource Source { get; set; }
	};
	
	public List<AudioEntity> AudioList = new List<AudioEntity>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
