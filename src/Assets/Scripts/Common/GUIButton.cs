using UnityEngine;
using System.Collections;

public abstract class GUIButton : MonoBehaviour {
	
	public GUITexture BaseTexture;
	public GUIButton Host;
	
	private bool onClick;
	private bool onHover;
	
	public GUISkin Skin;
	
	void OnGUI()
	{
		BaseTexture.enabled = false;
		
		GUI.skin = Skin;
		var camera = GameObject.FindObjectOfType (typeof(Camera)) as Camera;
		var n = Host.transform.localScale;
		var m = new Vector2(n.x * Screen.width, n.y * Screen.height);
		var y = Host.transform.localPosition;
		var z = new Vector2(y.x * Screen.width, (1 - y.y) * Screen.height);
		z -= m / 2;
		if (GUI.Button(new Rect(z.x, z.y, m.x, m.y), ""))
		{
			OnClick();
		}
	}
	
	public abstract void OnClick();
}
