﻿using UnityEngine;
using System.Collections;

public static class StageManager {
	
	public static ScoreData pScoreData;
	
	public struct ScoreData {
		public int iTrap { get; set; }
		public int iStudent { get; set; }
		public int iMan { get; set; }
		public int iWoman { get; set; }
		public int iSanta { get; set; }
		
		public int IntValue() {
			return iStudent * 50 +
					iMan * 500 +
					iWoman * 2000 +
					iSanta * 2000;
		}
		
		public void IncreaseOfType(int i) {
			switch (i) {
			case 0: // child, the old, abe; abe is never caught so not counted
				iTrap++;
				break;
			case 1:
				iStudent++;
				break;
			case 2:
				iMan++;
				break;
			case 3:
				iWoman++;
				break;
			case 4:
				iSanta++;
				break;
			default: // none of npc has this type value
				break;
			}
		}
	}

}
