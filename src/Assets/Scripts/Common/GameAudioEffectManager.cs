﻿using UnityEngine;
using System.Collections;

public class GameAudioEffectManager : MonoBehaviour {
	
	public AudioSource DashAudio;
	public AudioSource FailAudio;
	public AudioSource SuccAudio;
	public AudioSource DesuAudio;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Dash() {
		DashAudio.Play();
	}
	
	public void Fail() {
		FailAudio.Play();
	}
	
	public void Success() {
		SuccAudio.Play();
	}
	
	public void Die() {
		DesuAudio.Play();
	}
}
